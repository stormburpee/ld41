﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerEvent : MonoBehaviour {


    protected abstract void PlayerWalkedIntoTrigger();

    public void ActivateEvent() { PlayerWalkedIntoTrigger(); }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
            PlayerWalkedIntoTrigger();
    }

}
