﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    public TMP_Text clockText;
    public TMP_Text healthText;
    private float oldHealth;
    private float oldBaseHealth;

    public GameObject fade;

    public static UIManager instance;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Start()
    {
        HideFade();
    }

    void Update()
    {
        //clockText.text = WorldManager.instance.dayCycle.GetTimeString();
        UpdateUI();
    }

    void UpdateUI()
    {
        if(oldHealth != Player.instance.stats.health || oldBaseHealth != Player.instance.stats.GetBaseHealth())
        {
            oldHealth = Player.instance.stats.health;
            oldBaseHealth = Player.instance.stats.GetBaseHealth();
            healthText.text = oldHealth + "/" + Player.instance.stats.GetBaseHealth();
        }
    }

    public void ShowFade()
    {
        fade.SetActive(true);
    }

    public void HideFade()
    {
        fade.SetActive(false);
    }

}