﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity
{
    
    public BaseEnemyStats baseEnemyStats;
    public Personality personality;
    public bool seenTarget;
    protected int lastSawTarget = 0;
    public bool isBasicEnemy;
    public List<TriggerEvent> eventsOnDeath = new List<TriggerEvent>();
    public List<ItemDrop> itemsToDropOnDeath = new List<ItemDrop>();

    public bool randomizeItems = false;

    new void Start()
    {
        base.Start();
        seenTarget = false;
        personality.Initialize();

        if (isBasicEnemy)
        {
            StartCoroutine(BasicEnemyBrain());
            StartCoroutine(Basic_EnemyAttack());
        }
        else
            StartCoroutine(EnemyBrain());
        StartCoroutine(AnimateEntity());

        if(randomizeItems)
        {
            if (Random.Range(0, 100) < 50)
            {
                int randomItemID = Random.Range(0, ItemDatabase.GetItemCount());
                while (ItemDatabase.IsItemBannedFromDropping(randomItemID))
                {
                    randomItemID = Random.Range(0, ItemDatabase.GetItemCount());
                }
                int amount = Random.Range(1, ItemDatabase.getItemByItemID(randomItemID).GetMaxStackSize());
                ItemDrop id = new ItemDrop();
                id.amount = amount;
                id.itemID = randomItemID;
                itemsToDropOnDeath.Add(id);
            }
        }
    }

    new void Update()
    {
        base.Update();
    }

    public bool TargetInSight()
    {
        if (personality.GetAgressivePersonality().target == null) return false;
        for(int x = -baseEnemyStats.vision; x <= baseEnemyStats.vision; x++)
        {
            for(int y = -baseEnemyStats.vision; y <= baseEnemyStats.vision; y++)
            {
                if(CleanVector2(new Vector2(transform.position.x + x, transform.position.y + y)) == CleanVector2(personality.GetAgressivePersonality().target.transform.position))
                {
                    seenTarget = true;
                    return true;
                }
            }
        }
        return false;
    }

    public void WalkToRandomDirection()
    {
        Vector2 WalkDirection = new Vector2();
        int rWalk = Random.Range(0, 4);
        switch (rWalk)
        {
            case 0:
                WalkDirection = new Vector2(0, 1);
                break;
            case 1:
                WalkDirection = new Vector2(1, 0);
                break;
            case 2:
                WalkDirection = new Vector2(0, -1);
                break;
            case 3:
                WalkDirection = new Vector2(-1, 0);
                break;
        }
        LookInDirection(WalkDirection);
        Move(WalkDirection);
    }

    public Vector2 Basic_CalculateDirectionToTarget(bool overkill)
    {
        Vector2 tPos = CleanVector2(personality.GetAgressivePersonality().target.transform.position);
        Vector2 mPos = CleanVector2(transform.position);
        int nPosX = ((tPos.x != mPos.x) ? (tPos.x > mPos.x ? 1 : -1) : 0);
        int nPosY = ((tPos.y != mPos.y) ? (tPos.y > mPos.y ? 1 : -1) : 0);
        Vector2 nPos = new Vector2(nPosX, nPosY);
        
        //Overkill, oh well rip. That's what you get for being basic
        if(!CanMove(nPos) && overkill)
        {
            float dPosX = Vector2.Distance(new Vector2(mPos.x, 0), new Vector2(tPos.x, 0));
            float dPosY = Vector2.Distance(new Vector2(0, mPos.y), new Vector2(0, tPos.y));
            nPos = dPosX > dPosY ? new Vector2(nPosX, 0) : new Vector2(0, nPosY);
            if(!CanMove(nPos))
                nPos = dPosX < dPosY ? new Vector2(nPosX, 0) : new Vector2(0, nPosY);
        }

        return nPos;
    }

    public Vector2 Basic_CalculateDirectionToTarget()
    {
        return Basic_CalculateDirectionToTarget(true);
    }

    public void Basic_MoveTowardsTarget()
    {
        Move(Basic_CalculateDirectionToTarget());
    }

    public void CalculateDirectionToTarget()
    {

    }

    public void MoveTowardsTarget()
    {

    }

    public override void Die()
    {
        base.Die();
        foreach (TriggerEvent e in eventsOnDeath)
            e.ActivateEvent();
        foreach (ItemDrop id in itemsToDropOnDeath)
        {
            ItemPickupManager.SpawnItemPickup(transform.position, id);
        }
        itemsToDropOnDeath = new List<ItemDrop>();
    }

    public void Basic_MoveAwayfromTarget()
    {
        Vector2 DirectionAway = -Basic_CalculateDirectionToTarget();
        Move(DirectionAway);
    }

    public void Basic_AttackTarget()
    {
        if(baseEnemyStats.attackType == EnemyAttackType.Ranged && isAlive)
        {
            Vector2 DirectionToTarget = Basic_CalculateDirectionToTarget(false);
            ProjectileManager.SpawnNewProjectile((Vector2)transform.position, DirectionToTarget, baseEnemyStats.attackItemID, baseEnemyStats.attackSpeed, this as Entity);
        } else if(baseEnemyStats.attackType == EnemyAttackType.Melee && isAlive)
        {
            if (Vector2.Distance(transform.position, Player.instance.transform.position) < 1.6)
                Player.instance.TakeHealth(baseEnemyStats.baseMeleeAttack);
        }
    }

    IEnumerator BasicEnemyBrain()
    {
        while(isAlive)
        {
            if(TargetInSight())
            {
                lastSawTarget = 0;
            } else if(lastSawTarget != -1) {
                lastSawTarget++;
                if (lastSawTarget > 3)
                {
                    lastSawTarget = -1;
                    seenTarget = false;
                }
            }
            if(seenTarget && Vector2.Distance(CleanVector2(transform.position), CleanVector2(personality.GetAgressivePersonality().target.transform.position)) > baseEnemyStats.range + 0.5f)
            {
                Basic_MoveTowardsTarget();
            } else
            {
                float moveRandomChance = MathUtility.Map(personality.GetWandererPersonality().personalityValue, 0, 5, 0, 90);
                if(Random.Range(0, 100) < moveRandomChance)
                {
                    WalkToRandomDirection();
                }
            }
            yield return new WaitForSeconds(baseEnemyStats.speed);
        }
    }
    IEnumerator Basic_EnemyAttack()
    {
        while (isAlive)
        {
            if (seenTarget && lastSawTarget == 0 && Vector2.Distance(CleanVector2(transform.position), CleanVector2(personality.GetAgressivePersonality().target.transform.position)) <= baseEnemyStats.range + 0.5f)
            {
                if (Vector2.Distance(CleanVector2(transform.position), CleanVector2(personality.GetAgressivePersonality().target.transform.position)) < baseEnemyStats.minAttackDistance)
                {
                    yield return new WaitForSeconds(baseEnemyStats.attackRate / 2);
                    Basic_MoveAwayfromTarget();
                }
                else
                {
                    yield return new WaitForSeconds(baseEnemyStats.attackRate);
                    Basic_AttackTarget();
                }
            }
            yield return new WaitForSeconds(baseEnemyStats.attackRate);
        }

    }
    IEnumerator EnemyBrain()
    {
        while(isAlive)
        {
            yield return new WaitForSeconds(baseEnemyStats.speed);
        }
    }

}

[System.Serializable]
public class Personality
{
    public float agression;
    public float fear;
    public List<Entity> scaredOf = new List<Entity>();
    public float desire;
    public List<int> itemsDesired = new List<int>();
    public float wander;
    public float misc;

    protected AgressivePersonality aPersonality;
    protected FearPersonality fPersonality;
    protected DesirePersonality dPersonality;
    protected List<Item> desires;
    protected WandererPersonality wPersonality;
    protected MiscPersonality mPersonality;

    public void Initialize()
    {
        desires = new List<Item>();
        foreach (int i in itemsDesired)
            desires.Add(ItemDatabase.getItemByItemID(i));

        aPersonality = new AgressivePersonality(agression, Player.instance);
        fPersonality = new FearPersonality(fear, scaredOf);
        dPersonality = new DesirePersonality(desire, desires);
        wPersonality = new WandererPersonality(wander);
        mPersonality = new MiscPersonality(misc);
    }

    public AgressivePersonality GetAgressivePersonality() { return aPersonality; }
    public FearPersonality GetFearPersonality() { return fPersonality; }
    public DesirePersonality GetDesirePersonality() { return dPersonality; }
    public WandererPersonality GetWandererPersonality() { return wPersonality; }
    public MiscPersonality GetMiscPersonality() { return mPersonality; }
}

public enum EnemyAttackType
{
    Ranged,
    Melee,
    Mixed
}
   
[System.Serializable]
public class BaseEnemyStats
{
    public int vision = 2;
    public float speed = 0.5f;
    public int memory = 3;
    public int range = 1;
    public EnemyAttackType attackType;
    public int attackItemID;
    public float attackSpeed;
    public float attackRate;
    public float minAttackDistance;
    public float baseMeleeAttack = 5;
}

[System.Serializable]
public class EnemyPersonality
{
    public float personalityValue;

    public EnemyPersonality(float personalityValue)
    {
        this.personalityValue = personalityValue;
    }
}

[System.Serializable]
public class AgressivePersonality : EnemyPersonality
{
    public Entity target;

    public AgressivePersonality(float personalityValue) : base(personalityValue) { }
    public AgressivePersonality(float personalityValue, Entity target) : base(personalityValue)
    {
        this.target = target;
    }
}

[System.Serializable]
public class FearPersonality : EnemyPersonality
{
    public List<Entity> scaredOf;

    public FearPersonality(float personalityValue) : base(personalityValue) { }
    public FearPersonality(float personalityValue, Entity scaredOf) : base(personalityValue)
    {
        this.scaredOf = new List<Entity>();
        this.scaredOf.Add(scaredOf);
    }
    public FearPersonality(float personalityValue, List<Entity> scaredOf) : base(personalityValue)
    {
        this.scaredOf = scaredOf;
    }
}

[System.Serializable]
public class DesirePersonality : EnemyPersonality
{
    public List<Item> desires;

    public DesirePersonality(float personalityValue) : base(personalityValue) { }
    public DesirePersonality(float personalityValue, Item desires) : base(personalityValue)
    {
        this.desires = new List<Item>();
        this.desires.Add(desires);
    }
    public DesirePersonality(float personalityValue, List<Item> desires) : base(personalityValue)
    {
        this.desires = desires;
    }
}

[System.Serializable]
public class WandererPersonality : EnemyPersonality
{
    public WandererPersonality(float personalityValue) : base (personalityValue) { }
}

[System.Serializable]
public class MiscPersonality : EnemyPersonality
{
    public MiscPersonality(float personalityValue) : base(personalityValue) { }
}

[System.Serializable]
public class ItemDrop
{
    public int itemID;
    public int amount = 1;
}