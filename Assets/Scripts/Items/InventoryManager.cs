﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryManager : MonoBehaviour {
    
    public Inventory armourInventory;
    public Transform t_mainInventory;
    public Transform t_secondInventory;

    private bool inventoryOpen;
    protected bool secondaryInventoryOpen;
    protected Inventory secondInventory;

    public delegate void InventoryOpen();
    public static event InventoryOpen OnInventoryOpened;

    public delegate void InventoryClose();
    public static event InventoryClose OnInventoryClosed;

    public List<Transform> hotbarItemSlots = new List<Transform>();
    public List<Transform> inventoryItemSlots = new List<Transform>();
    public Sprite selectedSlot;
    public Sprite defaultSlot;
    protected int selectedItem;

    public Transform canvas;

    public Sprite boxHover;
    public Sprite boxDefault;
    public Sprite boxDown;

    public Transform mouseSprite;

    public TMP_Text tooltipName;
    public TMP_Text tooltipDesc;

    public static InventoryManager instance;
    
    public bool hasItemInMouse = false;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Start () {
        Player.instance.GetInventory().inventorySize = 10;

        armourInventory = new Inventory();
        armourInventory.inventorySize = 6;

        t_mainInventory.gameObject.SetActive(false);

        //mainInventory.AddItemToInventory(ItemDatabase.getItemByItemID(0));
    }
	
	void Update () {
        if (inventoryOpen)
            DrawInventory();

        if (!inventoryOpen && Input.GetKeyDown(KeyCode.E) && !GameManager.instance.gamePaused)
            OpenInventory();
        if (inventoryOpen && Input.GetKeyDown(KeyCode.Escape))
            CloseInventory();

        if(!inventoryOpen)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                selectedItem = 0;
            if (Input.GetKeyDown(KeyCode.Alpha2))
                selectedItem = 1;
            if (Input.GetKeyDown(KeyCode.Alpha3))
                selectedItem = 2;
            if (Input.GetKeyDown(KeyCode.Alpha4))
                selectedItem = 3;
            if (Input.GetKeyDown(KeyCode.Alpha5))
                selectedItem = 4;

            if(Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                int modifier = 0;
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                    modifier = -1;
                if (Input.GetAxis("Mouse ScrollWheel") < 0)
                    modifier = 1;
                selectedItem += modifier;
                if (selectedItem < 0) selectedItem = 4;
                if (selectedItem > 4) selectedItem = 0;
            }
        }

        DrawInventoryBar();
    }

    public void OpenInventory()
    {
        OpenInventory(Player.instance.GetInventory());
    }

    public void InventorySlotClicked(int slotLocation)
    {
        
    }

    public void InventorySlotMouseDown(int slotLocation)
    {
        inventoryItemSlots[slotLocation].GetComponent<Image>().sprite = boxDown;
    }

    public void InventorySlotMouseUp(int slotLocation)
    {
        //inventoryItemSlots[slotLocation].GetComponent<Image>().sprite = boxDefault;
        bool hadItemInMouse = false;
        if (hasItemInMouse) hadItemInMouse = true;
        bool didFindItem = false;
        if (Player.instance.GetInventory().GetItemAtLocation(slotLocation) != null)
        {
            if (Player.instance.GetInventory().GetItemAtLocation(-1) != null) Player.instance.GetInventory().GetItemAtLocation(-1).locationInInventory = -2;
            hasItemInMouse = true;
            Player.instance.GetInventory().GetItemAtLocation(slotLocation).locationInInventory = -1;
            didFindItem = true;
            if (Player.instance.GetInventory().GetItemAtLocation(-2) != null)
                Player.instance.GetInventory().GetItemAtLocation(-2).locationInInventory = slotLocation;
        }
        if (hadItemInMouse && !didFindItem)
        {
            int location = -1;
            Player.instance.GetInventory().GetItemAtLocation(location).locationInInventory = slotLocation;
            hasItemInMouse = false;
        }
    }

    public void InventorySlotMouseEnter(int slotLocation)
    {
        inventoryItemSlots[slotLocation].GetComponent<Image>().sprite = boxHover;
        if (Player.instance.GetInventory().GetItemAtLocation(slotLocation) != null)
        {
            tooltipName.text = Player.instance.GetInventory().GetItemAtLocation(slotLocation).item.GetItemName();
            tooltipDesc.text = Player.instance.GetInventory().GetItemAtLocation(slotLocation).item.GetItemDescription();
        }

    }

    public void InventorySlowMouseExit(int slotLocation)
    {
        inventoryItemSlots[slotLocation].GetComponent<Image>().sprite = boxDefault;
    }

    public void DeleteButtonClicked()
    {
        if(hasItemInMouse && Player.instance.GetInventory().GetItemAtLocation(-1) != null)
        {
            InventoryItem i = Player.instance.GetInventory().GetItemAtLocation(-1);
            ItemDrop id = new ItemDrop();
            id.amount = i.amount;
            id.itemID = i.item.GetItemID();
            ItemPickupManager.SpawnItemPickup((Vector2)Player.instance.transform.position + Player.instance.GetVectorDirection() * 2, id);
            Player.instance.GetInventory().RemoveItemAtLocation(-1, 50000);
            hasItemInMouse = false;
        }
    }

    public void OpenInventory(Inventory inventory)
    {
        inventoryOpen = true;
        if(inventory != Player.instance.GetInventory())
        {
            secondInventory = inventory;
            secondaryInventoryOpen = true;
        } else
        {
            secondInventory = null;
            secondaryInventoryOpen = false;
        }
        if (OnInventoryOpened != null)
            OnInventoryOpened();
        UIManager.instance.ShowFade();
        t_mainInventory.gameObject.SetActive(true);
    }

    public void CloseInventory()
    {
        inventoryOpen = false;
        secondInventory = null;
        secondaryInventoryOpen = false;
        if (OnInventoryClosed != null)
            OnInventoryClosed();
        UIManager.instance.HideFade();
        t_mainInventory.gameObject.SetActive(false);
        mouseSprite.GetComponent<Image>().color = new Color(1, 1, 1, 0);

        if (hasItemInMouse && Player.instance.GetInventory().GetItemAtLocation(-1) != null)
            Player.instance.GetInventory().GetItemAtLocation(-1).locationInInventory = Player.instance.GetInventory().GetNextFreeInventorySpace();
    }

    public InventoryItem GetCurrentSelectedItem()
    {
        InventoryItem item = Player.instance.GetInventory().GetItemAtLocation(selectedItem);
        return item;
    }

    void DrawInventoryBar()
    {
        for(int i = 0; i < 5; i++)
        {
            InventoryItem item = Player.instance.GetInventory().GetItemAtLocation(i);
            if(selectedItem == i)
            {
                hotbarItemSlots[i].GetComponent<Image>().sprite = selectedSlot;
            } else
            {
                hotbarItemSlots[i].GetComponent<Image>().sprite = defaultSlot;
            }

            if (item != null)
            {
                Sprite itemSprite = item.item.GetItemSprite();
                hotbarItemSlots[i].GetChild(0).GetComponent<Image>().sprite = itemSprite;
                hotbarItemSlots[i].GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                if(item.amount > 1)
                {
                    hotbarItemSlots[i].GetChild(2).gameObject.SetActive(true);
                    hotbarItemSlots[i].GetChild(2).GetComponent<TMP_Text>().text = item.amount.ToString();
                }
                else
                {
                    hotbarItemSlots[i].GetChild(2).gameObject.SetActive(false);
                }
            }
            else
            {
                hotbarItemSlots[i].GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
                hotbarItemSlots[i].GetChild(2).gameObject.SetActive(false);
            }
        }
    }

    void DrawInventory()
    {
        for(int i = 0; i < 10; i++)
        {
            InventoryItem item = Player.instance.GetInventory().GetItemAtLocation(i);
            if(item != null)
            {
                Sprite itemSprite = item.item.GetItemSprite();
                inventoryItemSlots[i].GetChild(0).GetComponent<Image>().sprite = itemSprite;
                inventoryItemSlots[i].GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                if(item.amount > 1)
                {
                    inventoryItemSlots[i].GetChild(1).gameObject.SetActive(true);
                    inventoryItemSlots[i].GetChild(1).GetComponent<TMP_Text>().text = item.amount.ToString();
                } else
                {
                    inventoryItemSlots[i].GetChild(1).gameObject.SetActive(false);
                }
            } else
            {
                inventoryItemSlots[i].GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
                inventoryItemSlots[i].GetChild(1).gameObject.SetActive(false);
            }
        }
        if(hasItemInMouse && Player.instance.GetInventory().GetItemAtLocation(-1) != null)
        {
            InventoryItem i = Player.instance.GetInventory().GetItemAtLocation(-1);
            Vector2 pos = new Vector2();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas as RectTransform, Input.mousePosition, Camera.main, out pos);
            mouseSprite.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            mouseSprite.GetComponent<Image>().sprite = i.item.GetItemSprite();
            mouseSprite.position = canvas.TransformPoint(pos);
        } else
        {
            mouseSprite.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        }
    }
}
