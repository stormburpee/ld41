﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogManager : MonoBehaviour {

    public Image characterImageSlot;
    public TMP_Text dialogText;
    public TMP_Text characterNameText;

    public Transform t_dialog;

    private List<Dialog> dialogMessagesToDisplay = new List<Dialog>();
    private bool showingDialog = false;

    public delegate void DialogOpened();
    public static event DialogOpened OnDialogOpened;
    public delegate void DialogClosed();
    public static event DialogClosed OnDialogFinished;

    public float textWriteSpeed = 0.05f;
    private bool isWriting = false;
    private bool skip = false;

    public static DialogManager instance;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Start () {
        t_dialog.gameObject.SetActive(false);
	}
	
	void Update () {
        if (showingDialog)
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if(!isWriting)
                    DisplayNextDialog();
                else
                {
                    skip = true;
                }
            }
	}

    public void ShowDialog(Entity dialogFrom, string dialog)
    {
        dialogMessagesToDisplay.Add(new Dialog(dialogFrom, dialog));
        if(!showingDialog)
        {
            showingDialog = true;
            t_dialog.gameObject.SetActive(true);
            DisplayNextDialog();
            if (OnDialogOpened != null)
                OnDialogOpened();
        }
    }

    public void ShowDialog(Entity dialogFrom, List<string> dialog)
    {
        foreach (string s in dialog)
            ShowDialog(dialogFrom, s);
    }

    public void DisplayNextDialog()
    {
        if(dialogMessagesToDisplay.Count > 0)
        {
            Dialog d = dialogMessagesToDisplay[0];
            dialogMessagesToDisplay.Remove(d);

            //dialogText.text = d.dialogMessage;
            characterNameText.text = d.dialogFrom.entityName;
            characterImageSlot.sprite = d.dialogFrom.southSprites[0];
            StartCoroutine(WriteText(d.dialogMessage));
        } else
        {
            showingDialog = false;
            t_dialog.gameObject.SetActive(false);
            if (OnDialogFinished != null)
                OnDialogFinished();
        }
    }

    IEnumerator WriteText(string text)
    {
        isWriting = true;
        int lengthOfText = text.Length;
        string originalText = text;
        string displayText = "";
        while(lengthOfText > 0 && !skip)
        {
            char ns = text[0];
            displayText += ns;
            text = text.Substring(1, lengthOfText - 1);
            lengthOfText = text.Length;
            dialogText.text = displayText;
            yield return new WaitForSeconds(textWriteSpeed);
        }
        skip = false;
        dialogText.text = originalText;
        isWriting = false;
    }
}

[System.Serializable]
public class Dialog
{
    public Entity dialogFrom;
    public string dialogMessage;

    public Dialog(Entity dialogFrom, string dialogMessage)
    {
        this.dialogFrom = dialogFrom;
        this.dialogMessage = dialogMessage;
    }
}