﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WorldManager : MonoBehaviour {

    public DayCycle dayCycle;

    public static WorldManager instance;

    public int gameDay = 1;

    void Awake()
    {
        if (!instance) instance = this;
    }

    void Start () {
        /*dayCycle.Init();
        dayCycle.ResumeTime();

        DayCycle.OnDayChanged += OnDayChanged;
        DayCycle.OnHourChanged += OnHourChanged;*/
	}
	
	void Update () {
        //dayCycle.Update();
	}

    void OnDayChanged()
    {
        gameDay += 1;
    }

    private void OnDisable()
    {
        DayCycle.OnDayChanged -= OnDayChanged;
        DayCycle.OnHourChanged -= OnHourChanged;
    }

    void OnHourChanged(int hour)
    {
        if(hour == 7)
        {
            CameraManager.instance.TransitionToDay();
        }
        if(hour == 21)
        {
            CameraManager.instance.TransitionToNight();
        }
    }
}
