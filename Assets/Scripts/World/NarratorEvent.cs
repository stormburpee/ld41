﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarratorEvent : TriggerEvent
{
    public string NarratorMessage;

    protected override void PlayerWalkedIntoTrigger()
    {
        NarratorManager.instance.ShowNarration(NarratorMessage);
        GameObject.Destroy(this.gameObject);
    }

}
