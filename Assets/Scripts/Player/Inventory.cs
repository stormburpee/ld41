﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory {

    public List<InventoryItem> itemsInInventory = new List<InventoryItem>();
    public int inventorySize = 10;

    public Inventory() { }

    public Inventory(List<InventoryItem> itemsInInventory)
    {
        this.itemsInInventory = itemsInInventory;
    }

    public Inventory(List<Item> items)
    {
        foreach(Item i in items)
        {
            int itemLoc = GetNextFreeInventorySpace();
            if(itemLoc != -1)
            {
                InventoryItem item = new InventoryItem(i, itemLoc);
                itemsInInventory.Add(item);
            }
        }
    }

    public int GetNextFreeInventorySpace()
    {
        for(int i = 0; i < inventorySize; i++)
        {
            bool itemInPlace = false;
            foreach(InventoryItem item in itemsInInventory)
            {
                if(item.locationInInventory == i)
                {
                    itemInPlace = true;
                    break;
                }
            }
            if (!itemInPlace) return i;
        }
        return -1;
    }

    public bool IsInventoryFull()
    {
        return (GetNextFreeInventorySpace() == -1);
    }

    public bool CanFitItem(Item item, int amount)
    {
        foreach(InventoryItem i in itemsInInventory)
        {
            if(i.item == item)
            {
                if (i.item.GetMaxStackSize() - i.amount >= amount) return true;
            }
        }
        return !IsInventoryFull();
    }

    public bool CanFitItem(Item item) { return CanFitItem(item, 1); }

    public int MaxItemsCanFit(Item item)
    {
        if (!IsInventoryFull())
            return item.GetMaxStackSize();
        int maxAm = 0;
        foreach(InventoryItem i in itemsInInventory)
        {
            if(i.item == item)
            {
                maxAm += i.item.GetMaxStackSize() - i.amount;
            }
        }
        return maxAm;
    }

    public List<InventoryItem> GetAmmoInInventory(int ammoFor)
    {
        List<InventoryItem> ammo = new List<InventoryItem>();
        foreach(InventoryItem i in itemsInInventory)
        {
            if(i.item.GetType() == typeof(AmmoItem))
            {
                AmmoItem ammoItem = i.item as AmmoItem;
                if (ammoItem.UsedIn(ammoFor)) ammo.Add(i);
            }
        }
        return ammo;
    }

    public bool AddItemToInventory(Item i) { return AddItemToInventory(i, 1); }

    public bool AddItemToInventory(Item i, int amount)
    {
        int amLeftToAllocate = amount;
        foreach(InventoryItem ii in itemsInInventory)
        {
            if(ii.item == i)
            {
                while(ii.item.GetMaxStackSize() - ii.amount > 0)
                {
                    amLeftToAllocate--;
                    ii.amount += 1;
                    if (amLeftToAllocate <= 0) return true;
                }
            }
        }
        int itemLoc = GetNextFreeInventorySpace();

        if (itemLoc == -1) return false;

        InventoryItem item = new InventoryItem(i, itemLoc, amLeftToAllocate);
        itemsInInventory.Add(item);

        return true;
    }

    public bool RemoveItemAtLocation(int itemLocation, int amount)
    {
        InventoryItem iToRemove = null;
        foreach(InventoryItem i in itemsInInventory)
        {
            if(i.locationInInventory == itemLocation)
            {
                iToRemove = i;
                break;
            }
        }
        if(iToRemove != null)
        {
            if(iToRemove.Remove(amount))
                itemsInInventory.Remove(iToRemove);
            return true;
        }
        return false;
    }
    public bool RemoveItemAtLocation(int itemLocation) { return RemoveItemAtLocation(itemLocation, 1); }

    public bool RemoveItemFromInventory(Item i, int amount)
    {
        InventoryItem iToRemove = null;
        foreach(InventoryItem item in itemsInInventory)
        {
            if(item.item == i) {
                iToRemove = item;
                break;
            }
        }
        if(iToRemove != null)
        {
            if(iToRemove.Remove(amount))
                itemsInInventory.Remove(iToRemove);
            return true;
        }
        return false;
    }
    public bool RemoveItemFromInventory(Item i) { return RemoveItemFromInventory(i, 1); }

    public InventoryItem GetItemAtLocation(int location)
    {
        foreach(InventoryItem item in itemsInInventory)
        {
            if (item.locationInInventory == location)
                return item;
        }
        return null;
    }

}

[System.Serializable]
public class InventoryItem
{
    public Item item;
    public int locationInInventory;
    public int amount;

    public InventoryItem(Item item, int locationInInventory, int amount)
    {
        this.item = item;
        this.locationInInventory = locationInInventory;
        this.amount = amount;
    }

    public InventoryItem(Item item, int locationInInventory)
    {
        this.item = item;
        this.locationInInventory = locationInInventory;
        this.amount = 1;
    }
    
    public void ChangeLocation(int locationInInventory)
    {
        this.locationInInventory = locationInInventory;
    }

    public bool Add()
    {
        return Add(1);
    }

    public bool Add(int amount)
    {
        this.amount += amount;
        if (this.amount > item.GetMaxStackSize()) return true;
        return false;
    }

    public bool Remove()
    {
        return Remove(1);
    }

    public bool Remove(int amount)
    {
        this.amount -= amount;
        if (this.amount <= 0) return true;
        return false;
    }
}
