﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndTrigger : TriggerEvent {

    protected override void PlayerWalkedIntoTrigger()
    {
        SceneManager.LoadScene(3);
    }
}
