﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    protected Vector2 direction;
    protected int maxAge;
    protected int currentAge;
    protected AmmoItem item;
    protected float speed;
    protected float aSpeed;
    protected Entity protectedEntity;

    public void Initialize(Vector2 direction, int maxAge, AmmoItem item, float speed, Entity protectedEntity)
    {
        this.direction = direction;
        this.currentAge = 0;
        this.maxAge = maxAge;
        this.item = item;
        this.speed = speed;
        GetComponentInChildren<SpriteRenderer>().sprite = item.GetItemSprite();
        this.protectedEntity = protectedEntity;

        int rotAmm = direction.x != 0 ? (direction.x < 0 ? 0 : 2) : (direction.y < 0 ? 1 : -1);

        transform.GetChild(0).rotation = Quaternion.Euler(new Vector3(0, 0, 45 + (rotAmm * 90)));

        StartCoroutine(MoveProjectile());
    }

    public void Initialize(Vector2 direction, int maxAge, AmmoItem item)
    {
        Initialize(direction, maxAge, item, 1f, null);
    }

    IEnumerator MoveProjectile()
    {
        while(currentAge < maxAge)
        {
            Vector2 startPos = transform.position;
            float moveIncrement = 0f;
            transform.GetChild(0).localScale = (currentAge % 2 == 1) ? new Vector2(0.25f, 0.25f) : new Vector2(0.5f, 0.5f);

            while(moveIncrement < 1f)
            {
                moveIncrement += (1f / speed) * Time.deltaTime;
                if (moveIncrement > 1f) moveIncrement = 1f;
                transform.position = startPos + (direction * moveIncrement);
                yield return null;
            }
            
            yield return null;
            currentAge++;
        }
        GameObject.Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Entity>() != protectedEntity && collision.GetComponent<Projectile>() == null)
        {
            ProjectileManager.ProjectileEnded(transform.position, collision, item);
            GameObject.Destroy(this.gameObject);
        }
    }
}

public static class ProjectileManager
{
    public static GameObject SpawnNewProjectile(Vector2 startPos, Vector2 direction, int projectileID, float projectileSpeed, Entity entityToBeProtected)
    {
        GameObject projectileGO = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Projectile"), startPos, Quaternion.identity);
        projectileGO.GetComponent<Projectile>().Initialize(direction, 10, ItemDatabase.getItemByItemID(projectileID) as AmmoItem, projectileSpeed, entityToBeProtected);
        return projectileGO;
    }

    public static void ProjectileEnded(Vector2 position, Collider2D collision, AmmoItem item)
    {
        Entity e = collision.GetComponent<Entity>();
        if (item.GetAmmoEffects() == AmmoEffects.Damage)
            e.TakeHealth(item.GetBaseAttack());
    }
}