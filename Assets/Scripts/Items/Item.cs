﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item {

    protected int itemID;
    protected string itemName;
    protected string itemDescription;
    protected string itemFlavourText;
    protected int maxStackSize;
    protected int tilesetLocation;

    public Item(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize, int tilesetLocation)
    {
        this.itemID = itemID;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemFlavourText = itemFlavourText;
        this.maxStackSize = maxStackSize;
        this.tilesetLocation = tilesetLocation;
    }

    public Item(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize)
    {
        this.itemID = itemID;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemFlavourText = itemFlavourText;
        this.maxStackSize = maxStackSize;
        this.tilesetLocation = itemID;
    }

    public Item(int itemID, string itemName, string itemDescription, string itemFlavourText)
    {
        this.itemID = itemID;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemFlavourText = itemFlavourText;
        this.maxStackSize = 64;
        this.tilesetLocation = itemID;
    }

    public Item(int itemID, string itemName, string itemDescription)
    {
        this.itemID = itemID;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemFlavourText = "";
        this.maxStackSize = 64;
        this.tilesetLocation = itemID;
    }

    public int GetItemID()
    {
        return itemID;
    }

    public string GetItemName()
    {
        return itemName;
    }

    public string GetItemDescription()
    {
        return itemDescription;
    }

    public string GetItemFlavourText()
    {
        return itemFlavourText;
    }

    public string GetItemSpriteLocation()
    {
        return "items/tileset";
    }

    public Sprite GetItemSprite()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>(GetItemSpriteLocation());
        //Debug.Log(sprites);
        //Debug.Log(sprites.Length);
        return sprites[tilesetLocation];
    }

    public int GetMaxStackSize()
    {
        return maxStackSize;
    }
	
}

public class WearableItem : Item
{
    protected WearableLocation wearableLocation;

    public WearableItem(int itemID, string itemName, string itemDescription, string itemFlavourText, WearableLocation location) : base(itemID, itemName, itemDescription, itemFlavourText)
    {
        this.wearableLocation = location;
        this.maxStackSize = 1;
    }

    public WearableLocation GetWearableLocation()
    {
        return this.wearableLocation;
    }
}

public class UseableItem : Item
{
    public UseableItem(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize, int tilesetLocation) : base(itemID, itemName, itemDescription, itemFlavourText, maxStackSize, tilesetLocation)
    {

    }

    public UseableItem(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize) : base(itemID, itemName, itemDescription, itemFlavourText, maxStackSize)
    {

    }
}

public class PotionItem : UseableItem
{

    public enum PotionEffects
    {
        Health,
        IncreaseBaseHealth
    }

    protected PotionEffects effect;
    protected float effectAmount;

    public PotionItem(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize, int tilesetLocation, PotionEffects effects, float potionEffectsAmount) : base(itemID, itemName, itemDescription, itemFlavourText, maxStackSize, tilesetLocation)
    {
        this.effect = effects;
        effectAmount = potionEffectsAmount;
    }

    public PotionEffects GetPotionEfect()
    {
        return effect;
    }

    public float GetPotionEffectAmount()
    {
        return effectAmount;
    }
}

public class ScrollItem : UseableItem
{

    public enum ScrollEffects
    {
        Damage,
        Health,
        Speed
    }

    public enum ScrollTarget
    {
        Ranged,
        Self
    }

    public enum ScrollDesign
    {
        Fire,
        Magic,
        Water
    }

    protected ScrollEffects effect;
    protected float baseEffectsAmount;

    protected ScrollTarget target;
    protected ScrollDesign design;

    protected int itemIDOfOrb;

    public ScrollItem(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize, int tilesetLocation, ScrollEffects effects, float baseEffectsAmount, ScrollTarget target, ScrollDesign design) : base(itemID, itemName, itemDescription, itemFlavourText, maxStackSize, tilesetLocation)
    {
        this.effect = effects;
        this.baseEffectsAmount = baseEffectsAmount;
        this.target = target;
        this.design = design;
        this.itemIDOfOrb = 9;
        this.maxStackSize = maxStackSize;
    }

    public ScrollItem(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize, int tilesetLocation, ScrollEffects effects, float baseEffectsAmount, ScrollTarget target, ScrollDesign design, int itemIDOfOrb) : base(itemID, itemName, itemDescription, itemFlavourText, maxStackSize, tilesetLocation)
    {
        this.effect = effects;
        this.baseEffectsAmount = baseEffectsAmount;
        this.target = target;
        this.design = design;
        this.itemIDOfOrb = itemIDOfOrb;
        this.maxStackSize = maxStackSize;
    }

    public ScrollEffects GetPotionEfect()
    {
        return effect;
    }

    public int GetItemIDOfOrb() { return itemIDOfOrb; }

    public float GetBaseEffectAmount()
    {
        return baseEffectsAmount;
    }
    
    public ScrollTarget GetScrollTarget()
    {
        return target;
    }
    
    public ScrollDesign GetScrollDesign()
    {
        return design;
    }
}

public class PlaceableItem : Item
{
    public PlaceableItem(int itemID, string itemName, string itemDescription, string itemFlavourText, int maxStackSize) : base(itemID, itemName, itemDescription, itemFlavourText, maxStackSize)
    {

    }

    public string GetPlaceableLocation()
    {
        return "placeable/" + itemID;
    }

    public Sprite GetPlaceableSprite()
    {
        return Resources.Load<Sprite>(GetPlaceableLocation());
    }
}

public class WeaponItem : Item
{
    protected float baseAttack;

    public WeaponItem(int itemID, string itemName, string itemDescription, string itemFlavourText, float baseAttack) : base(itemID, itemName, itemDescription, itemFlavourText)
    {
        this.baseAttack = baseAttack;
        this.maxStackSize = 1;
    }

    public WeaponItem(int itemID, string itemName, string itemDescription, string itemFlavourText, float baseAttack, int tileLocation) : base(itemID, itemName, itemDescription, itemFlavourText, 1, tileLocation)
    {
        this.baseAttack = baseAttack;
        this.maxStackSize = 1;
    }

    public float GetBaseAttack()
    {
        return baseAttack;
    }
}

public class RangedWeaponItem : WeaponItem
{

    protected bool infiniteAmmo;
    protected bool isAmmo;
    protected float weaponSpeed;

    public RangedWeaponItem(int itemID, string itemName, string itemDescription, string itemFlavourText, float baseAttack, bool infiniteAmmo, float weaponSpeed) : base(itemID, itemName, itemDescription, itemFlavourText, baseAttack)
    {
        this.infiniteAmmo = infiniteAmmo;
        this.weaponSpeed = weaponSpeed;
        this.maxStackSize = 1;
    }

    public RangedWeaponItem(int itemID, string itemName, string itemDescription, string itemFlavourText, float baseAttack, int tileLocation, bool infiniteAmmo, float weaponSpeed) : base(itemID, itemName, itemDescription, itemFlavourText, baseAttack, tileLocation)
    {
        this.infiniteAmmo = infiniteAmmo;
        this.weaponSpeed = weaponSpeed;
        this.maxStackSize = 1;
    }

    public bool IsInfiniteAmmo()
    {
        return infiniteAmmo;
    }

    public float GetWeaponSpeed()
    {
        return weaponSpeed;
    }
}

public class AmmoItem : WeaponItem
{

    public enum AmmoType
    {
        Useable,
        Throwable
    }

    protected AmmoEffects effects;

    protected AmmoType ammoType;

    protected List<int> usedIn = new List<int>();
    public AmmoItem(int itemID, string itemName, string itemDescription, string itemFlavourText, float baseAttack, int tileLocation, int maxStack, List<int> usedIn, AmmoType ammoType) : base(itemID, itemName, itemDescription, itemFlavourText, baseAttack, tileLocation)
    {
        this.maxStackSize = maxStack;
        this.usedIn = usedIn;
        this.ammoType = ammoType;
        this.effects = AmmoEffects.Damage;
    }

    public AmmoItem(int itemID, string itemName, string itemDescription, string itemFlavourText, float baseAttack, int tileLocation, int maxStack, List<int> usedIn, AmmoType ammoType, AmmoEffects effects) : base(itemID, itemName, itemDescription, itemFlavourText, baseAttack, tileLocation)
    {
        this.maxStackSize = maxStack;
        this.usedIn = usedIn;
        this.ammoType = ammoType;
        this.effects = effects;
    }

    public bool UsedIn(int itemID)
    {
        return usedIn.Contains(itemID);
    }

    public AmmoType GetAmmoType()
    {
        return ammoType;
    }

    public AmmoEffects GetAmmoEffects() { return effects; }
}


public enum WearableLocation
{
    Head,
    Chest,
    Legs,
    Boots,
    Necklace,
    Ring
}

public enum AmmoEffects
{
    Damage,
    Health
}