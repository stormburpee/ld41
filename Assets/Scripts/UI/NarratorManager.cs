﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NarratorManager : MonoBehaviour {

    public static NarratorManager instance;
    public TMP_Text narratorText;
    public GameObject narratorBar;

    public float textWriteSpeed = 0.0015f;
    public float hideNarrationAfter = 1.5f;
    private bool isWriting = false;
    private List<string> messages = new List<string>();

    public List<string> damageMessages = new List<string>();
    public List<string> deathMessages = new List<string>();

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Start()
    {
        HideNarrationBar();
    }

    public void ShowNarration(string message)
    {
        messages.Add(message);
        if(!isWriting)
            StartCoroutine(WriteMessage());
    }

    public void ShowDamageMessage()
    {
        ShowNarration(damageMessages[Random.Range(0, damageMessages.Count)]);
    }

    public void ShowDeathMessage()
    {
        ShowNarration(deathMessages[Random.Range(0, deathMessages.Count)]);
    }

    void ShowNarrationBar()
    {
        narratorBar.SetActive(true);
    }

    void HideNarrationBar()
    {
        narratorBar.SetActive(false);
        narratorText.text = "";
    }
    
    void OnNarrationEnded()
    {
        if (messages.Count > 0)
            StartCoroutine(WriteMessage());
    }

    IEnumerator WriteMessage()
    {
        HideNarrationBar();
        ShowNarrationBar();
        isWriting = true;
        string actualMessage = "Narrator: " + messages[0];
        messages.RemoveAt(0);
        int lengthOfText = actualMessage.Length;
        string omsg = actualMessage;
        string displayText = "";
        while(lengthOfText > 0)
        {
            char ns = actualMessage[0];
            displayText += ns;
            actualMessage = actualMessage.Substring(1, lengthOfText - 1);
            narratorText.text = displayText;
            lengthOfText = actualMessage.Length;
            yield return new WaitForSeconds(textWriteSpeed);
        }
        narratorText.text = omsg;
        yield return new WaitForSeconds(hideNarrationAfter);
        isWriting = false;
        HideNarrationBar();
        OnNarrationEnded();
    }

}
