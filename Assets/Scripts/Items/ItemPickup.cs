﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour {

    public int itemID;
    public int amount;

    private void Start()
    {
        GetComponentInChildren<SpriteRenderer>().sprite = ItemDatabase.getItemByItemID(itemID).GetItemSprite();
    }

    public void Initialize(int itemID, int amount)
    {
        this.itemID = itemID;
        this.amount = amount;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Player>() != null)
        {
            if(collision.GetComponent<Player>().PickupItem(this))
                GameObject.Destroy(this.gameObject);
        }
    }

}

public static class ItemPickupManager
{
    public static void SpawnItemPickup(Vector2 atPos, ItemDrop itemDrop)
    {
        GameObject droppedItem = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("ItemPickup"), atPos, Quaternion.identity);
        droppedItem.GetComponent<ItemPickup>().Initialize(itemDrop.itemID, itemDrop.amount);
    }
}
