﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public bool DebugMode = false;

    public bool gamePaused = false;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Start()
    {
        InventoryManager.OnInventoryOpened += OnInventoryOpened;
        InventoryManager.OnInventoryClosed += OnInventoryClosed;
        DialogManager.OnDialogOpened += OnDialogOpened;
        DialogManager.OnDialogFinished += OnDialogClosed;


        //Intro();
    }

    void Intro()
    {
        List<string> introductionString = new List<string>();
        introductionString.Add("After many, many, MANY years of fighting the good fight, being the hero that they needed, but no body deserved, I have finally decided to hang up the cape. I mean, I'm pretty sure I'm allowed to right? Look how old I am!");
        introductionString.Add("I found the most isolated island, away from all the action that the modern day heroes have to fight and decided to settle down in a nice little farm.");
        introductionString.Add("Retirement is going to be so good. I think I might head into town, and see what the local bar is offering up to drink!");

        DialogManager.instance.ShowDialog(Player.instance, introductionString);
    }

    void OnDisable()
    {
        InventoryManager.OnInventoryOpened -= OnInventoryOpened;
        InventoryManager.OnInventoryClosed -= OnInventoryClosed;
        DialogManager.OnDialogOpened -= OnDialogOpened;
        DialogManager.OnDialogFinished -= OnDialogClosed;
    }

    void Update()
    {
        
    }

    void OnInventoryOpened()
    {
        gamePaused = true;
    }

    void OnInventoryClosed()
    {
        gamePaused = false;
    }

    void OnDialogOpened()
    {
        gamePaused = true;
    }

    void OnDialogClosed()
    {
        gamePaused = false;
    }
}
