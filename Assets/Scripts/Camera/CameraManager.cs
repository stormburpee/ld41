﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public enum CameraStates
    {
        FollowPlayer,
        LookAtThing
    }
    

    public enum UpdateTypes
    {
        FixedUpdate,
        Update,
        LateUpdate,
        Manual
    }

    public UpdateTypes updateType;

    public CameraStates cameraState;
    public float cameraMoveSpeed = 3f;

    private Transform target;
    private Camera cam;

    public float dayZoom = 2.5f;
    public float nightZoom = 4.5f;
    public float zoomSpeed;

    public static CameraManager instance;

    private Vector3 oldTargetPos;
    private Vector3 oldCamPos;

    public bool useOverKill = false;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

	void Start () {
        if (!target)
            target = GameObject.FindGameObjectWithTag("Player").transform;
        cameraState = CameraStates.FollowPlayer;
        cam = GetComponent<Camera>();
        TransitionToDay();
        //if (updateType == UpdateTypes.Manual)
        //    StartCoroutine(SmoothFollow());
	}
	
	void Update () {
		
        if(cameraState == CameraStates.FollowPlayer && updateType == UpdateTypes.Update)
        {
            Vector3 targetPos = new Vector3(target.position.x, target.position.y, transform.position.z);
            //transform.position = SmoothApproach(oldCamPos, oldTargetPos, targetPos, cameraMoveSpeed);
            //oldCamPos = transform.position;
            //oldTargetPos = targetPos;
            Vector3 camVel = new Vector3();
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref camVel, cameraMoveSpeed);
        }
	}

    void FixedUpdate()
    {

        if (cameraState == CameraStates.FollowPlayer && updateType == UpdateTypes.FixedUpdate)
        {
            Vector3 targetPos = new Vector3(target.position.x, target.position.y, transform.position.z);
            //transform.position = SmoothApproach(oldCamPos, oldTargetPos, targetPos, cameraMoveSpeed);
            //oldCamPos = transform.position;
            //oldTargetPos = targetPos;
            Vector3 camVel = new Vector3();
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref camVel, cameraMoveSpeed);
        }

        if (useOverKill)
            transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
    }

    void LateUpdate()
    {

        if (cameraState == CameraStates.FollowPlayer && updateType == UpdateTypes.LateUpdate)
        {
            Vector3 targetPos = new Vector3(target.position.x, target.position.y, transform.position.z);
            //transform.position = SmoothApproach(oldCamPos, oldTargetPos, targetPos, cameraMoveSpeed);
            //oldCamPos = transform.position;
            //oldTargetPos = targetPos;
            Vector3 camVel = new Vector3();
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref camVel, cameraMoveSpeed);
        }
    }

    Vector3 SmoothApproach(Vector3 pastPosition, Vector3 pastTargetPosition, Vector3 targetPosition, float speed)
    {
        float t = Time.deltaTime * speed;
        Vector3 v = (targetPosition - pastTargetPosition) / t;
        Vector3 f = pastPosition - pastTargetPosition + v;
        return targetPosition - v + f * Mathf.Exp(-t);
    }

    public void TransitionToDay()
    {
        StartCoroutine(TransitionCameraZoom(dayZoom));
    }

    public void TransitionToNight()
    {
        StartCoroutine(TransitionCameraZoom(nightZoom));
    }

    public void TransitionToZoom(float zoom)
    {
        StartCoroutine(TransitionCameraZoom(zoom));
    }

    public IEnumerator TransitionCameraZoom(float zoom)
    {
        float increment = 0;
        float startZoom = cam.orthographicSize;

        while (increment < 1f)
        {
            increment += (1f / zoomSpeed) * Time.deltaTime;
            if (increment > 1)
                increment = 1;

            cam.orthographicSize = startZoom +  ((zoom - startZoom) * increment);

            yield return null;
        }

        cam.orthographicSize = zoom;
    }

    public IEnumerator SmoothFollow()
    {
        while(true)
        {
            float increment = 0;
            Vector3 startPos = transform.position;
            Vector3 targetPos = new Vector3(Player.instance.transform.position.x, Player.instance.transform.position.y, transform.position.z);

            while (increment < 1f)
            {
                increment += (1f / cameraMoveSpeed) * Time.deltaTime;
                if (increment > 1)
                    increment = 1;

                transform.position = startPos + ((targetPos - startPos) * increment);

                yield return null;
            }
            yield return null;
        }
    }
}
