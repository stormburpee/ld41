﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tile
{
    protected int x, y;
    protected bool solid;

    public Tile(int x, int y)
    {
        this.x = x;
        this.y = y;
        this.solid = false;
    }

    public Tile(int x, int y, bool solid)
    {
        this.x = x;
        this.y = y;
        this.solid = solid;
    }

    public Vector2 GetTilePos()
    {
        return new Vector2(x, y);
    }

    public bool IsSolid()
    {
        return solid;
    }
}

public enum TileType
{
    Nothing = 0,
    Hallway = 1,
    Wall = 2,
    Door = 3
}