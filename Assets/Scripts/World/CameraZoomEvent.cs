﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomEvent : TriggerEvent
{
    public float CameraZoom;
    public bool destroyWhenDone;

    protected override void PlayerWalkedIntoTrigger()
    {
        CameraManager.instance.TransitionToZoom(CameraZoom);
        if(destroyWhenDone)
            GameObject.Destroy(this.gameObject);
    }

}
