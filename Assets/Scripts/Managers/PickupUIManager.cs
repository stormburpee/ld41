﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PickupUIManager : MonoBehaviour {

    public TMP_Text itemNameText;
    public TMP_Text itemDescText;
    public TMP_Text itemFlavourText;
    public Transform pickupTransform;

    private List<Item> itemsToShow = new List<Item>();
    private bool showingItem = false;
    public float hideDisplayAfter = 2.5f;

    public static PickupUIManager instance;

    private void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Start () {
		
	}

    public void ShowPickedupItem(Item item)
    {
        itemsToShow.Add(item);
        if(!showingItem)
        {
            pickupTransform.gameObject.SetActive(true);
            StartCoroutine(DisplayItem());
        }
    }

    public void ClosePickedupItem()
    {
        pickupTransform.gameObject.SetActive(false);
        itemNameText.text = "";
        itemDescText.text = "";
        itemFlavourText.text = "";
        showingItem = false;
    }

    public void ItemStoppedDisplaying()
    {
        if (itemsToShow.Count > 0)
            StartCoroutine(DisplayItem());
        else
            ClosePickedupItem();
    }
	
	IEnumerator DisplayItem()
    {
        showingItem = true;
        Item i = itemsToShow[0];
        itemsToShow.RemoveAt(0);
        itemNameText.text = i.GetItemName();
        itemDescText.text = i.GetItemDescription();
        itemFlavourText.text = i.GetItemFlavourText();
        yield return new WaitForSeconds(hideDisplayAfter);
        ItemStoppedDisplaying();
    }
}
