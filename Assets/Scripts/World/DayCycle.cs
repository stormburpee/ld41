﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DayCycle {

    public Color[] skyLight = new Color[24];
    private Color currentLight;
    private Color nextLight;
    private float r, g, b;

    public bool modifyAmbientLight;

    public Light sunLight;

    public float customTime = 12f;
    public float customTimeScale = 3f;

    private int hour;
    private int minute;

    private bool progressTime;

    public delegate void HourChanged(int hour);
    public static event HourChanged OnHourChanged;

    public delegate void DayChanged();
    public static event DayChanged OnDayChanged;

    public void Init()
    {
        hour = 8;
        minute = 0;
        WorldManager.instance.StartCoroutine(HandleTime());
    }

	public void Update()
    {
        if(progressTime)
            customTime += (Time.deltaTime) / (customTimeScale);
    }

    public void PauseTime()
    {
        progressTime = false;
    }

    public void ResumeTime()
    {
        progressTime = true;
    }

    public string GetTimeString()
    {
        return ((hour < 10) ? "0" + hour.ToString() : hour.ToString()) + ":" + ((minute < 10) ? "0" + minute.ToString() : minute.ToString());
    }

    public IEnumerator HandleTime()
    {
        while(true)
        {
            int oldHour = hour;
            if (customTime >= 24)
            {
                customTime = 0;
                OnDayChanged();
            }

            hour = Mathf.FloorToInt(customTime);
            minute = Mathf.FloorToInt((customTime - hour) * 60);

            if (oldHour != hour)
            {
                if(OnHourChanged != null)
                    OnHourChanged(hour);
            }

            currentLight = skyLight[hour];
            if (hour < 23) nextLight = skyLight[hour + 1];
            else nextLight = skyLight[0];

            r = currentLight.r - (((currentLight.r - nextLight.r) / 60) * minute);
            g = currentLight.g - (((currentLight.g - nextLight.g) / 60) * minute);
            b = currentLight.b - (((currentLight.b - nextLight.b) / 60) * minute);

            sunLight.color = new Color(r, g, b, 1);

            if (modifyAmbientLight) RenderSettings.ambientLight = sunLight.color;

            yield return null;
        }
    }
}
