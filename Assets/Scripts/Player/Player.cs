﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity {
    
    public enum PlayerStates
    {
        Exploring,
        Building,
        Talking
    }

    public PlayerStates playerState;

    [HideInInspector]
    public static Player instance;

    protected int lastMovementKey = -1;
    protected Vector2 desiredMove;

    public List<GameObject> slashes = new List<GameObject>();

    new void Awake()
    {
        base.Awake();
        if (!instance)
            instance = this;
    }

    new void Start () {
        base.Start();

        playerState = PlayerStates.Exploring;
        StartCoroutine(AnimatePlayer());
        StartCoroutine(HandlePlayerMovement());
        stats.health = 25;
        stats.Initialize();
	}

    new void Update()
    {
        base.Update();
        if (isAlive && !GameManager.instance.gamePaused)
        {
            HandleMovement();
            HandleInteraction();
        }
    }
	
	void FixedUpdate () {
        
	}

    void HandleMovement()
    {
        if(playerState == PlayerStates.Exploring)
        {
            if(Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                desiredMove = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                LookInDirection(desiredMove);
                //Move(desiredMove);
            }
        }
    }

    void HandleInteraction()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InventoryItem currentItem = InventoryManager.instance.GetCurrentSelectedItem();
            if (currentItem != null)
            {
                if (currentItem.item.GetType() == typeof(RangedWeaponItem))
                {
                    RangedWeaponItem rWep = currentItem.item as RangedWeaponItem;
                    List<InventoryItem> ammo = inventory.GetAmmoInInventory(currentItem.item.GetItemID());
                    if (ammo.Count > 0)
                    {
                        AmmoItem i = ammo[0].item as AmmoItem;
                        ProjectileManager.SpawnNewProjectile(transform.position, GetVectorDirection(), i.GetItemID(), rWep.GetWeaponSpeed(), this);
                        inventory.RemoveItemFromInventory(ammo[0].item);
                    }
                }
                else if (currentItem.item.GetType() == typeof(WeaponItem))
                {
                    ShowSlash();
                    WeaponItem wItem = currentItem.item as WeaponItem;
                    Vector2 direction = GetVectorDirection();
                    RaycastHit2D hit = Physics2D.Raycast(((Vector2)new Vector2(transform.position.x + direction.x / 1.5f, transform.position.y + direction.y / 1.5f)), direction, 0.5f);
                    if(hit.collider != null && hit.collider.GetComponent<Entity>() != null)
                    {
                        Entity e = hit.collider.GetComponent<Entity>();
                        e.TakeHealth(wItem.GetBaseAttack());
                    }
                }
                else if (currentItem.item.GetType() == typeof(ScrollItem))
                {
                    ScrollItem i = currentItem.item as ScrollItem;
                    switch(i.GetScrollDesign())
                    {
                        case ScrollItem.ScrollDesign.Fire:
                            ProjectileManager.SpawnNewProjectile(transform.position, GetVectorDirection(), i.GetItemIDOfOrb(), 0.2f, this);
                            break;
                        case ScrollItem.ScrollDesign.Magic:
                            if(i.GetScrollTarget() == ScrollItem.ScrollTarget.Self) {
                                if(i.GetPotionEfect() == ScrollItem.ScrollEffects.Speed)
                                {
                                    stats.walkSpeed -= 0.05f;
                                    if (stats.walkSpeed < 0.05f) stats.walkSpeed = 0.05f;
                                }
                            } else
                                ProjectileManager.SpawnNewProjectile(transform.position, GetVectorDirection(), i.GetItemIDOfOrb(), 0.2f, this);
                            break;
                    }
                    inventory.RemoveItemFromInventory(i);
                }
                else if (currentItem.item.GetType() == typeof(PotionItem))
                {
                    PotionItem i = currentItem.item as PotionItem;
                    switch(i.GetPotionEfect())
                    {
                        case PotionItem.PotionEffects.Health:
                            AddHealth(i.GetPotionEffectAmount());
                            break;
                        case PotionItem.PotionEffects.IncreaseBaseHealth:
                            stats.AddToBaseHealth(i.GetPotionEffectAmount());
                            break;
                    }
                    inventory.RemoveItemFromInventory(i);
                }
                else if (currentItem.item.GetType() == typeof(AmmoItem))
                {
                    AmmoItem i = currentItem.item as AmmoItem;
                    if(i.GetAmmoType() == AmmoItem.AmmoType.Throwable)
                    {
                        ProjectileManager.SpawnNewProjectile(transform.position, GetVectorDirection(), i.GetItemID(), 0.15f, this);
                        inventory.RemoveItemFromInventory(i);
                    }
                }
            }
        }
    }

    bool IsWalkInputDown()
    {
        return (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0);
    }

    void ShowSlash()
    {
        Animator slashAnimator = null;
        switch(direction)
        {
            case 0:
                slashAnimator = slashes[0].GetComponent<Animator>();
                break;
            case 1:
                slashAnimator = slashes[1].GetComponent<Animator>();
                break;
            case 2:
                slashAnimator = slashes[2].GetComponent<Animator>();
                break;
            case 3:
                slashAnimator = slashes[3].GetComponent<Animator>();
                break;
        }
        if(slashAnimator != null)
        {
            slashAnimator.Play("slash", -1, 0f);
            slashAnimator.Play("slash");
        }
    }

    IEnumerator HandlePlayerMovement()
    {
        while(true)
        {
            while(IsWalkInputDown() && !isWalking && CanMove(desiredMove) && !GameManager.instance.gamePaused)
            {
                isWalking = true;
                
                Vector2 startPos = transform.position;
                Vector2 movingto = desiredMove;
                moveIncrement = 0;
                float speed = CalculateSpeed();
                
                while (moveIncrement < 1f)
                {
                    moveIncrement += (1f / speed) * Time.deltaTime;
                    if (moveIncrement > 1)
                        moveIncrement = 1;
                    transform.position = startPos + (movingto * moveIncrement);
                    yield return null;
                }
                isWalking = false;
                UpdateSprite();
            }
            yield return null;
        }
    }

    public IEnumerator MoveCameraTo(Vector3 targetPosition)
    {
        Transform t_cam = CameraManager.instance.transform;
        float camSpeed = CameraManager.instance.cameraMoveSpeed;
        Vector3 startPos = t_cam.position;
        Vector3 movement = targetPosition - startPos;

        float increment = 0f;
        if(camSpeed != 0)
        {
            while(increment < 1f)
            {
                increment += (1f / camSpeed) * Time.deltaTime;
                if (increment > 1) increment = 1;
                t_cam.position = startPos + (movement * increment);
                yield return null;
            }
        }
        t_cam.position = targetPosition;
    }

    public Vector2 GetDesiredPos() { return desiredMove; }

    IEnumerator AnimatePlayer()
    {
        while(true)
        {
            animKeyFrame = 0;
            while(IsWalkInputDown())
            {
                animKeyFrame++;
                if (animKeyFrame > 3)
                    animKeyFrame = 0;
                UpdateSprite();
                yield return new WaitForSeconds(0.1f);
            }
            yield return null;
        }
    }
}
