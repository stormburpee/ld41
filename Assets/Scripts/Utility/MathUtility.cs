﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathUtility
{

    public static float Map(float value, float a0, float a1, float b0, float b1)
    {
        return b0 + (b1 - b0) * ((value - a0) / (a1 - a0));
    }

    public static int Map(int value, int a0, int a1, int b0, int b1)
    {
        return b0 + (b1 - b0) * ((value - a0) / (a1 - a0));
    }

    public static float Map(float value, Vector2 a, Vector2 b)
    {
        return Map(value, a.x, a.y, b.x, b.y);
    }

}
