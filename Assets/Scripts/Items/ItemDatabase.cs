﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ItemDatabase {

    private static Item[] items = new Item[]
    {
        new WeaponItem(0, "Your Old Long Sword", "Generally used for kiling things.", "This sword is visibly ruined, from your past battles.", 5),
        new PotionItem(1, "Basic Health Potion", "Drink this to regain 5hp.", "The smell is bad, the taste is bad, the effect? It's ok.", 10, 16, PotionItem.PotionEffects.Health, 5),
        new PotionItem(2, "Carrot", "Eat this to regain 2hp.", "Mum always said to eat healthy. But a carrot? Really?", 10, 2, PotionItem.PotionEffects.Health, 2),
        new ScrollItem(3, "Basic Fire Scroll", "Use this to shoot fire in front of you.", "I can barely read the text on this!", 1, 3, ScrollItem.ScrollEffects.Damage, 5, ScrollItem.ScrollTarget.Ranged, ScrollItem.ScrollDesign.Fire, 12),
        new PotionItem(4, "Health Potion", "Use this to regain 10hp.", "This one doesn't taste so bad!", 10, 1, PotionItem.PotionEffects.Health, 10),
        new ScrollItem(5, "Basic Speed Scroll", "Use this to boost your speed!", "How does this work?!", 1, 3, ScrollItem.ScrollEffects.Speed, 2, ScrollItem.ScrollTarget.Self, ScrollItem.ScrollDesign.Magic),
        new RangedWeaponItem(6, "Bow", "Use this to shoot arrows!", "I'm probably going to need arrows for this..", 5, 4, false, 0.2f),
        new AmmoItem(7, "Basic Arrow", "Can be used in bows", "The tip of this arrow is quite blunt", 5, 5, 64, new List<int>(){6, 13, 14}, AmmoItem.AmmoType.Useable),
        new AmmoItem(8, "Throwing Star", "Throw this at enemies", "Ouch! These edges hurt in my pocket", 3, 7, 16, new List<int>(){}, AmmoItem.AmmoType.Throwable),
        new AmmoItem(9, "Dark Orb", "[PROJECTILE]", "I don't know how I got this in my inventory?", 5, 8, 1, new List<int>(){10}, AmmoItem.AmmoType.Throwable),
        new ScrollItem(10, "Scroll of the Dark Arts", "Use this to summon a Dark Orb", "The dark arts? Sounds cool", 1, 3, ScrollItem.ScrollEffects.Damage, 5, ScrollItem.ScrollTarget.Ranged, ScrollItem.ScrollDesign.Magic, 9),
        new PotionItem(11, "Magical Carrot", "Eat this to add 5hp to your base health!", "Is it safe to eat, if it's glowing?", 5, 2, PotionItem.PotionEffects.IncreaseBaseHealth, 5),
        new AmmoItem(12, "Fire Orb", "[PROJECTILE]", "I don't know how I got this in my inventory?", 10, 9, 1, new List<int>(3), AmmoItem.AmmoType.Throwable),
        new RangedWeaponItem(13, "Dark Bow", "A bow used by the darkest of evils", "This thing looks pretty cool!", 10, 10, false, 0.15f),
        new RangedWeaponItem(14, "Light Bow", "A bow used by the Holiest of goods", "This doesn't look as cool", 10, 11, false, 0.15f),
        new AmmoItem(15, "Throwing Knife", "Throw this at enemies", "It's pretty hard to throw accurately?", 3, 12, 16, new List<int>(), AmmoItem.AmmoType.Throwable, AmmoEffects.Damage),
        new AmmoItem(16, "Dark Throwing Knife", "A throwing knife used by the undead", "A much cooler throwing knife", 6, 13, 16, new List<int>(), AmmoItem.AmmoType.Throwable, AmmoEffects.Damage),
        new WeaponItem(17, "Light Great Sword", "A Great Sword wielded by heroes", "Jeez, this thing is heavy!", 15, 14),
        new WeaponItem(18, "Light Long Sword", "A Long Sword wielded by heroes", "Has a lot of sharp edges!", 10, 15),
        new WeaponItem(19, "Battle Axe", "A heavy axe, used in battles.", "I would not want to be on the receiving end of this!", 12, 17),
        new WeaponItem(20, "Dark Long Sword", "A Long Sword wielded by the undead", "This sword was originaly silver!", 18, 18),
        new AmmoItem(21, "Dark Arrow", "A arrow used by the undead!", "The aerodynamics seem off on this arrow?", 10, 19, 64, new List<int>(){6, 13, 14}, AmmoItem.AmmoType.Useable),
        new AmmoItem(22, "Fancy Throwing Knife", "Throw this at the undead!", "I think the cloth might get in the way", 6, 20, 16, new List<int>(), AmmoItem.AmmoType.Throwable, AmmoEffects.Damage),
        new WeaponItem(23, "Long Pike", "Used to poke enemies!", "Now this, I can get used too!", 10, 21),
        new PotionItem(24, "Banana", "Eat this to regain 2hp.", "B! A-N-A-N A!", 8, 22, PotionItem.PotionEffects.Health, 2),
        new PotionItem(25, "Watermelon", "Eat this to regain 2hp.", "How old is this watermelon?", 8, 23, PotionItem.PotionEffects.Health, 2),
        new ScrollItem(26, "Basic Speed Potion", "Drink this to increase your speed.", "I don't know if it's safe to drink this", 1, 24, ScrollItem.ScrollEffects.Speed, 0.05f, ScrollItem.ScrollTarget.Self, ScrollItem.ScrollDesign.Magic),
        new AmmoItem(27, "Bone", "You can throw this at enemies", "Ew! Why did I pick this up?", 4, 26, 8, new List<int>(), AmmoItem.AmmoType.Throwable, AmmoEffects.Damage),
        new PotionItem(28, "Eyeball", "You can eat this to regain 1hp.", "It's so squishy!", 2, 27, PotionItem.PotionEffects.Health, 1),
        new PotionItem(29, "Cheese", "Cheese! Eat this to regain 6hp.", "CHEEEEEEEEEEESE!", 8, 28, PotionItem.PotionEffects.Health, 6)
    };

    private static List<int> bannedDropIDs = new List<int>() { 9, 12, 0 };

    public static Item getItemByItemID(int id)
    {
        foreach(Item i in items)
        {
            if (i.GetItemID() == id)
                return i;
        }
        return null;
    }

    public static int GetItemCount() { return items.Length; }

    public static bool IsItemBannedFromDropping(int itemid) { return bannedDropIDs.Contains(itemid); }

}
