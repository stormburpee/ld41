﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Entity : MonoBehaviour {

    public string entityName;
    public EntityStats stats;
    public bool isAlive = true;

    protected Rigidbody2D rb;
    protected bool isRunning;
    protected int direction; // 0 = n, 1 = e, 2 = s, 3 = w
    protected float moveIncrement = 0;
    protected bool isWalking;

    protected Inventory inventory;
    protected bool takingDamage;

    public Sprite[] northSprites;
    public Sprite[] eastSprites;
    public Sprite[] southSprites;
    public Sprite[] westSprites;

    protected int animKeyFrame = 0;

    protected void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        stats.Initialize();
        inventory = new Inventory();
    }

    protected void Start () {
        
	}
	
	protected void Update () {
		if(isAlive && !GameManager.instance.gamePaused)
        {
        }

        if(GameManager.instance.DebugMode)
        {
            Debug_DrawWalkRays();
        }
	}

    protected Vector2 CleanVector2(Vector2 pos)
    {
        return new Vector2(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y));
    }
    
    public bool CanMove(Vector2 direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(((Vector2)new Vector2(transform.position.x + direction.x/1.5f, transform.position.y + direction.y/1.5f)), direction, 0.5f);
        return (hit.collider != null && !hit.collider.isTrigger) ? false : true;
    }

    public void Debug_DrawWalkRays()
    {
        for(int x = -1; x <= 1; x++)
        {
            for(int y = -1; y <= 1; y++)
            {
                Vector2 dir = new Vector2(x, y);
                Debug.DrawRay(((Vector2)new Vector2(transform.position.x + dir.x/1.5f, transform.position.y + dir.y/1.5f)), dir * 0.5f);
            }
        }
    }

    public void Move(Vector2 direction)
    {
        if(CanMove(CleanVector2(direction)) && !isWalking && !GameManager.instance.gamePaused)
        {
            Vector2 desiredPosition = new Vector2(Mathf.Floor(transform.position.x) + direction.x, Mathf.Floor(transform.position.y) + direction.y);
            LookAt(desiredPosition);
            //Debug.Log(direction + ", " + desiredPosition);
            StartCoroutine(MoveCoroutine(direction));
        }
        
    }

    public Vector2 GetVectorDirection()
    {
        switch(direction)
        {
            case 0:
                return new Vector2(0, 1);
            case 1:
                return new Vector2(1, 0);
            case 2:
                return new Vector2(0, -1);
            case 3:
                return new Vector2(-1, 0);
        }
        return Vector2.zero;
    }

    public void MoveTo(Vector2 pos)
    {

    }

    public int GetDirection() { return direction; }
    
    public void LookAt(Vector2 pos)
    {
        int disX = (pos.x > transform.position.x) ? (int)(pos.x - transform.position.x) : (int)(transform.position.x - pos.x);
        int disY = (pos.y > transform.position.y) ? (int)(pos.y - transform.position.y) : (int)(transform.position.y - pos.y);

        if(disX > disY)
        {
            direction = (pos.x > transform.position.x) ? 1 : 3;
        } else
        {
            direction = (pos.y > transform.position.y) ? 0 : 2;
        }

        UpdateSprite();
    }

    public void LookInDirection(Vector2 direction)
    {
        if(direction.x == -1)
        {
            this.direction = 3;
        } else if(direction.x == 1)
        {
            this.direction = 1;
        } else if(direction.y == -1)
        {
            this.direction = 2;
        } else if(direction.y == 1)
        {
            this.direction = 0;
        }

        UpdateSprite();
         
        //LookAt(new Vector2(Mathf.Floor(transform.position.x) + direction.x, Mathf.Floor(transform.position.y) + direction.y));
    }

    public void LookAt(Entity e) { LookAt(e.transform.position); }

    public void ChangeDirection(int direction)
    {
        this.direction = direction;
        UpdateSprite();
    }

    public void UpdateSprite()
    {
        switch(direction)
        {
            case 0:
                if (northSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = northSprites[animKeyFrame];
                break;
            case 1:
                if (eastSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = eastSprites[animKeyFrame];
                break;
            case 2:
                if (southSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = southSprites[animKeyFrame];
                break;
            case 3:
                if (westSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = westSprites[animKeyFrame];
                break;
        }
    }

    public void SetAnimSprite(int animKeyFrame)
    {
        switch (direction)
        {
            case 0:
                if (northSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = northSprites[animKeyFrame];
                break;
            case 1:
                if (eastSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = eastSprites[animKeyFrame];
                break;
            case 2:
                if (southSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = southSprites[animKeyFrame];
                break;
            case 3:
                if (westSprites[0] != null)
                    GetComponentInChildren<SpriteRenderer>().sprite = westSprites[animKeyFrame];
                break;
        }
    }

    public float CalculateSpeed()
    {
        return isRunning ? stats.runSpeed : stats.walkSpeed;
    }

    protected IEnumerator MoveCoroutine(Vector2 pos)
    {
        isWalking = true;

        Vector2 startPos = transform.position;
        moveIncrement = 0;
        float speed = CalculateSpeed();

        while(moveIncrement < 1f && isAlive)
        {
            moveIncrement += (1f / speed) * Time.deltaTime;
            if (moveIncrement > 1)
                moveIncrement = 1;
            transform.position = startPos + (pos * moveIncrement);
            yield return null;
        }
        if (Mathf.Abs(transform.position.x) - Mathf.Floor(transform.position.x) > 0.1 || Mathf.Abs(transform.position.y) - Mathf.Floor(transform.position.y) > 0.1 && isAlive)
            transform.position = CleanVector2(transform.position);
        isWalking = false;
    }

    public bool PickupItem(ItemPickup itemToPickup)
    {
        if(inventory.MaxItemsCanFit(ItemDatabase.getItemByItemID(itemToPickup.itemID)) > 0)
        {
            inventory.AddItemToInventory(ItemDatabase.getItemByItemID(itemToPickup.itemID), itemToPickup.amount);
            //I really don't like this, but there's 6 hours to go and I'm too tired to write an override/virtual function lol
            if (Player.instance as Entity == this) PickupUIManager.instance.ShowPickedupItem(ItemDatabase.getItemByItemID(itemToPickup.itemID));
            return true;
        }
        return false;
    }

    public Inventory GetInventory()
    {
        return inventory;
    }

    public virtual void Die()
    {
        if(Player.instance as Entity != this)
        {
            isAlive = false;
            StartCoroutine(DeathAnimation());
            if (Random.Range(0, 200) < 15)
            {
                NarratorManager.instance.ShowDeathMessage();
            }
        } else
        {
            StartCoroutine(DeathAnimation());
            
        }
    }

    public void TakeHealth(float amount)
    {
        this.stats.health -= amount;
        if (this.stats.health < 0)
            Die();
        else if (!takingDamage)
            StartCoroutine(AnimateDamage());

        if (Player.instance as Entity == this && isAlive)
        {
            if(Random.Range(0, 200) < 15)
            {
                NarratorManager.instance.ShowDamageMessage();
            }
        }
    }

    public void AddHealth(float amount)
    {
        this.stats.health += amount;
        if (this.stats.health > this.stats.GetBaseHealth())
            this.stats.health = this.stats.GetBaseHealth();
    }

    protected IEnumerator AnimateEntity()
    {
        while (true)
        {
            animKeyFrame = 0;
            while (isWalking)
            {
                animKeyFrame++;
                if (animKeyFrame > 3)
                    animKeyFrame = 0;
                UpdateSprite();
                yield return new WaitForSeconds(0.1f);
            }
            yield return null;
        }
    }

    protected IEnumerator DeathAnimation()
    {
        float elapsedTime = 0.0f;
        float totalTime = 0.5f;
        Quaternion startRot = transform.rotation;
        Quaternion toRot = Quaternion.Euler(0, 0, 90);
        Color deadCol = Color.red;
        deadCol.a = 0;
        while(elapsedTime < totalTime)
        {
            elapsedTime += Time.deltaTime;
            transform.rotation = Quaternion.Lerp(startRot, toRot, elapsedTime / totalTime);
            yield return null;
        }
        elapsedTime = 0.0f;
        totalTime = 0.5f;
        while(elapsedTime < totalTime)
        {
            elapsedTime += Time.deltaTime;
            GetComponentInChildren<SpriteRenderer>().color = Color.Lerp(Color.white, deadCol, elapsedTime / totalTime);
            yield return null;
        }
        if(Player.instance as Entity == this)
            SceneManager.LoadScene(2);
        GameObject.Destroy(this.gameObject);
    }

    protected IEnumerator AnimateDamage()
    {
        takingDamage = true;
        float elapsedTime = 0.0f;
        float totalTime = 0.5f;
        while(elapsedTime < totalTime)
        {
            elapsedTime += Time.deltaTime;
            Color fromColor = (elapsedTime > totalTime / 2) ? Color.red : Color.white; ;
            Color toColor = (elapsedTime < totalTime / 2) ? Color.red : Color.white;

            GetComponentInChildren<SpriteRenderer>().color = Color.Lerp(fromColor, toColor, (elapsedTime < totalTime / 2) ? elapsedTime / (totalTime / 2) : elapsedTime / totalTime);
            yield return null;
        }
        takingDamage = false;
    }
}

[System.Serializable]
public class EntityStats
{
    public float health = 100;
    private float baseHealth;

    public float walkSpeed = 0.3f;
    public float runSpeed = 0.15f;

    public void Initialize()
    {
        this.baseHealth = health;
    }

    public float GetBaseHealth() { return baseHealth; }
    public void SetBaseHealth(float baseHealth) { this.baseHealth = baseHealth; }
    public void AddToBaseHealth(float amount)
    {
        this.baseHealth += amount;
    }
}
